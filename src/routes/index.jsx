import { Route, Switch, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import organizationSlice from '../slices/organization'
import Snackbar from '@material-ui/core/Snackbar'
import { LoadingScreen } from '../components'
import Alert from '@material-ui/lab/Alert'
import commonSlice from '../slices/common'
import React, { useEffect } from 'react'
import { Error404 } from '../components'
import authSlice from '../slices/auth'
import { USER } from '../constants'
import getRoutes from './getRoutes'
import '../index.css'

export default React.memo(() => {
  const { isOpen, type, message } = useSelector(store => store.common.notification)
  const { isAuth, authChecked, user } = useSelector(store => store.auth)
  const dispatch = useDispatch()
  const location = useLocation()

  useEffect(() => {
    if (!authChecked) dispatch(authSlice.thunks.authenticate())
  }, [authChecked, dispatch])

  useEffect(() => {
    if ([USER.ROLES.ADMIN_VERIFIER, USER.ROLES.INDIVIDUAL_VERIFIER].includes(user.role) && user.organizationId) {
      dispatch(organizationSlice.thunks.getOrganizationById(user.organizationId))
    }
  }, [user.role, user.organizationId, dispatch])

  const handleClose = () => {
    dispatch(commonSlice.thunks.closeNotification())
  }

  if (!authChecked) return <LoadingScreen isFullHeight noSpacing />
  return (
    <>
      <Switch location={location}>
        {getRoutes({ isAuth, role: user.role })}
        <Route component={Error404} />
      </Switch>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isOpen}
        autoHideDuration={5000}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity={type}>
          {message}
        </Alert>
      </Snackbar>
    </>
  )
})
