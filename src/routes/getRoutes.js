import makeLazyComponent from '../utils/makeLazyComponent'
import { PrivateRoute } from '../components'
import { Route } from 'react-router-dom'
import { USER } from '../constants'
import React from 'react'

const pagesList = ['Auth', 'SuperAdmin', 'VerifierAdmin', 'RegularUser', 'IndividualVerifier']

const lazyPages = pagesList.reduce(
  (pageComps, page) => ({
    [page]: makeLazyComponent(() => import(`../containers/${page}`)),
    ...pageComps,
  }),
  {}
)

const toComponent = ({ auth = true, exact = true, path, Component }) => {
  const RouteComponent = auth ? PrivateRoute : Route
  return (
    <RouteComponent key={path} exact={exact} path={path}>
      {Component}
    </RouteComponent>
  )
}

export default ({ isAuth, role }) => {
  const pagesList = {
    [USER.ROLES.INDIVIDUAL_VERIFIER]: lazyPages.IndividualVerifier,
    [USER.ROLES.ADMIN_VERIFIER]: lazyPages.VerifierAdmin,
    [USER.ROLES.SUPER_ADMIN]: lazyPages.SuperAdmin,
    [USER.ROLES.USER]: lazyPages.RegularUser,
  }

  const protectedRoutes = [{ exact: false, path: '/', Component: pagesList[role] || pagesList[USER.ROLES.USER] }]
  const defaultsRoutes = [{ auth: false, exact: false, path: '/', Component: lazyPages.Auth }]

  const resRoutes = isAuth ? protectedRoutes : defaultsRoutes

  return resRoutes.map(toComponent)
}
