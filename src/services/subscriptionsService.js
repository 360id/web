import apis from 'common/apis'

export default {
  getSubscriptions: () => apis.subscription.getSubscriptions(),
  getBillingMethods: () => apis.subscription.getBillingMethods(),
}
