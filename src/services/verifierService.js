import baseService from './baseService'

export default {
  addVerifier: body => baseService('verifiers.createUser', body),
  updateVerifier: body => baseService('verifiers.updateUser', body),
  deleteVerifier: id => baseService('verifiers.deleteUser', { id }),
  getVerifierMetrics: () => baseService('verifiers.getVerifierMetrics'),
}
