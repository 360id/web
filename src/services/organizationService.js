import baseService from './baseService'

export default {
  getOrgVerifiersById: id => baseService('organizations.getOrganizationUsers', { id }),
  getOrganizationById: id => baseService('organizations.getOrganizationById', { id }),
  deleteOrganization: id => baseService('organizations.deleteOrganization', { id }),
  updateOrganization: body => baseService('organizations.updateOrganization', body),
  addOrganization: body => baseService('organizations.addOrganization', body),
  getOrganizations: () => baseService('organizations.getOrganizations'),
  getUsersMetrics: () => baseService('organizations.getUsersMetrics'),
}
