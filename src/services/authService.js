import baseService from './baseService'

export default {
  reset: (token, password) => baseService('users.reset', { token, password }),
  register: body => baseService('users.register', body),
  forgot: body => baseService('users.forgot', body),
  login: body => baseService('users.login', body),
  me: () => baseService('users.me'),
}
