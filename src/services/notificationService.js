import baseService from './baseService'

export default {
  getNotifications: () => baseService('notifications.getNotifications'),
  addNotification: body => baseService('notifications.addNotification', body),
  updateNotification: body => baseService('notifications.updateNotification', body),
  deleteNotification: id => baseService('notifications.deleteNotification', { id }),
}
