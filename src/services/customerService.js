import baseService from './baseService'

export default {
  getCustomers: () => baseService('customers.getCustomers'),
  addCustomer: body => baseService('customers.addCustomer', body),
  updateCustomer: body => baseService('customers.updateCustomer', body),
  removeCustomer: id => baseService('customers.removeCustomer', { id }),
  getCustomersMetrics: () => baseService('customers.getCustomersMetrics'),
  supportCustomer: body => baseService('customers.supportCustomer', body),
}
