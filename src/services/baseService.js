import { showNotification } from '../utils'
import { parseError } from '../utils'
import apis from 'common/apis'
import { get } from 'lodash'

export default async (path, params) => {
  try {
    const req = get(apis, path)
    if (req) {
      const res = await req(params)
      return res.data
    } else {
      throw new Error(`Endpoint: "${path}" not found`)
    }
  } catch (err) {
    showNotification({ message: parseError(err) })
    console.error(err)
    throw err
  }
}
