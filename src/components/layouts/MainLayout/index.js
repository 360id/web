import { makeStyles } from '@material-ui/core/styles'
import { Grid } from '@material-ui/core'
import PropTypes from 'prop-types'
import clname from 'classnames'
import React from 'react'

const useStyles = makeStyles(theme => ({
  fullHeight: {
    minHeight: '100vh',
  },
  defaultPadding: {
    padding: '20px 80px',
  },
}))

const layout = React.memo(({ children, isFullHeight, isDefaultPadding, noSpacing, className, ...rest }) => {
  const { fullHeight, defaultPadding } = useStyles()
  return (
    <Grid
      className={clname([className], { [fullHeight]: isFullHeight, [defaultPadding]: isDefaultPadding })}
      spacing={noSpacing ? 0 : 3}
      alignItems='center'
      justify='center'
      container
      {...rest}>
      {children}
    </Grid>
  )
})

layout.propTypes = {
  children: PropTypes.any.isRequired,
}

export default layout
