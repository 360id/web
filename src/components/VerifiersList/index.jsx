import organizationSlice from '../../slices/organization'
import AddEditVerifierModal from './AddEditVerifierModal'
import { useDispatch, useSelector } from 'react-redux'
import verifierSclice from '../../slices/verifier'
import React, { useEffect, useState } from 'react'
import { Table, TableHeader, Status } from '../'
import { Paper, Grid } from '@material-ui/core'
import { useParams } from 'react-router'

export default React.memo(() => {
  const { verifierLoading, verifierData } = useSelector(state => state.organization)
  const [isAddEditModal, $isAddEditModal] = useState(false)
  const user = useSelector(store => store.auth.user)
  const [rowToEdit, $rowToEdit] = useState(null)
  const dispatch = useDispatch()
  const params = useParams()

  const organizationId = params.organizationId || user.organizationId

  useEffect(() => {
    dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId))
  }, [dispatch, organizationId])

  const columns = React.useMemo(
    () => [
      {
        Header: 'First Name',
        accessor: 'data.firstName',
      },
      {
        Header: 'Last Name',
        accessor: 'data.lastName',
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'Phone',
        accessor: 'mobile',
      },
      {
        Header: 'Individual key',
        accessor: 'individualKey',
      },
      {
        Header: 'Access',
        accessor: 'role',
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ value }) => <Status value={value} />,
      },
    ],
    []
  )

  const onSearch = searchText => {
    dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId, { searchText }))
  }

  const onAddEdit = row => {
    $rowToEdit(row)
    $isAddEditModal(!isAddEditModal)
  }

  const actions = {
    Edit: row => onAddEdit(row.original),
    Delete: row => ({
      confirm: `Are you sure you want to delete ${row.original.data.firstName} ${row.original.data.lastName}`,
      func: () => dispatch(verifierSclice.thunks.deleteVerifier(row.original.id, organizationId)),
    }),
  }

  return (
    <Grid item xs={12}>
      {isAddEditModal && (
        <AddEditVerifierModal onCancel={() => onAddEdit()} rowToEdit={rowToEdit} organizationId={organizationId} />
      )}
      <TableHeader onSearch={onSearch} onAdd={() => onAddEdit()} onAddLabel='Add Verifier' />
      <Paper>
        <Table data={verifierData.orgUsers || []} loading={verifierLoading} actions={actions} columns={columns} />
      </Paper>
    </Grid>
  )
})
