import verifierSclice from '../../../slices/verifier'
import { Container, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { TextField } from 'formik-material-ui'
import { Formik, Field, Form } from 'formik'
import { useDispatch } from 'react-redux'
import React, { useRef } from 'react'
import { Modal } from '../../'
import { pick } from 'lodash'
import * as Yup from 'yup'

const useStyles = makeStyles(() => ({
  firstName: {
    marginTop: '0px',
  },
  addButtonsWrap: {
    padding: '0px 30px',
    width: '100%',
  },
  addButton: {
    textTransform: 'none',
    fontSize: '14px',
    width: '100%',
  },
}))

const validation = Yup.object().shape({
  firstName: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  email: Yup.string().email('Invalid Email').required('Required'),
})

export default React.memo(({ onCancel, rowToEdit, organizationId }) => {
  const dispatch = useDispatch()
  const classes = useStyles()
  const myFormRef = useRef()

  const renderButtons = () => (
    <div className={classes.addButtonsWrap}>
      <Button
        onClick={() => myFormRef.current.handleSubmit()}
        className={classes.addButton}
        variant='contained'
        color='primary'>
        {rowToEdit ? 'Save' : 'Add verifier and send key via email'}
      </Button>
    </div>
  )

  const onSubmit = async values => {
    let body = {
      data: pick(values, ['firstName', 'lastName']),
      email: values.email,
    }
    if (rowToEdit) {
      body = { ...body, id: rowToEdit.id }
    }
    const res = await dispatch(verifierSclice.thunks.addUpdateVerifier(body, organizationId))
    if (res) onCancel()
  }

  const getInitialValues = () => {
    if (rowToEdit) return { email: rowToEdit.email, ...pick(rowToEdit.data, ['firstName', 'lastName']) }
    return { firstName: '', lastName: '', email: '' }
  }

  const defaultProps = {
    component: TextField,
    margin: 'normal',
    fullWidth: true,
    type: 'text',
  }

  return (
    <Modal
      title={`${rowToEdit ? 'Edit' : 'Add'} Verifier`}
      customButtons={renderButtons()}
      onCancel={onCancel}
      buttonsPos='start'
      dividers={false}
      maxWidth='xs'
      fullWidth
      isOpen>
      <Container className={classes.container}>
        <Formik
          initialValues={getInitialValues()}
          validationSchema={validation}
          innerRef={myFormRef}
          onSubmit={onSubmit}>
          {() => (
            <Form noValidate>
              <Field {...defaultProps} label='First Name' name='firstName' className={classes.firstName} />
              <Field {...defaultProps} label='Last Name' name='lastName' />
              <Field {...defaultProps} label='Email Address' name='email' />
            </Form>
          )}
        </Formik>
      </Container>
    </Modal>
  )
})
