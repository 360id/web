import { useDispatch, useSelector } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import authSlice from '../../slices/auth'
import React from 'react'

export default React.memo(({ component: Component, children, ...rest }) => {
  const auth = useSelector(state => state.auth)
  const dispatch = useDispatch()

  if (!auth.isAuth) {
    dispatch(authSlice.thunks.logout())
    return <Redirect to='/login' />
  }

  if (Component) {
    // client wants to render Route via component prop
    return <Route {...rest} component={Component} />
  } else {
    // client wants to render Route via children function
    return <Route {...rest}>{children}</Route>
  }
})
