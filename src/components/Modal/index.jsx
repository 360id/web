import {
  DialogContent as MuiDialogContent,
  DialogActions as MuiDialogActions,
  DialogTitle as MuiDialogTitle,
  Typography,
  IconButton,
  Dialog,
  Button,
} from '@material-ui/core'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import PropTypes from 'prop-types'
import React from 'react'

const DialogTitle = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    marginTop: '10px',
  },
  title: {
    paddingLeft: '24px',
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
}))(props => {
  const { children, classes, onClose, ...other } = props
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant='h6' className={classes.title}>
        {children}
      </Typography>
      {onClose ? (
        <IconButton aria-label='close' className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  )
})

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent)

const DialogActions = withStyles(theme => ({
  root: {
    justifyContent: ({ pos }) => pos,
    padding: theme.spacing(1),
    marginBottom: '20px',
    margin: 0,
  },
}))(MuiDialogActions)

const useStyles = makeStyles(() => ({
  cancelBtn: {
    marginRight: '16px',
  },
}))

const CustomModal = React.memo(
  ({
    customButtons,
    onCancelLabel,
    onOkLoading,
    buttonsPos,
    onOkLabel,
    dividers,
    children,
    onCancel,
    isOpen,
    title,
    onOk,
    ...rest
  }) => {
    const classes = useStyles()
    return (
      <Dialog maxWidth='md' onClose={onCancel} open={isOpen} {...rest}>
        {title && <DialogTitle onClose={onCancel}>{title}</DialogTitle>}
        <DialogContent dividers={dividers}>{children}</DialogContent>
        <DialogActions pos={buttonsPos === 'start' ? 'flex-start' : 'flex-end'}>
          {customButtons || (
            <div>
              <Button onClick={onCancel} variant='contained' className={classes.cancelBtn}>
                {onCancelLabel || 'Cancel'}
              </Button>
              <Button onClick={onOk} color='primary' variant='contained' disabled={onOkLoading}>
                {onOkLoading ? 'Loading...' : onOkLabel || 'Save'}
              </Button>
            </div>
          )}
        </DialogActions>
      </Dialog>
    )
  }
)

CustomModal.propTypes = {
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  customButtons: PropTypes.node,
  onCancelLabel: PropTypes.node,
  onOkLoading: PropTypes.bool,
  onOkLabel: PropTypes.node,
  dividers: PropTypes.bool,
  title: PropTypes.node,
  onOk: PropTypes.func,
}

CustomModal.defaultProps = {
  dividers: true,
  buttonsPos: 'end',
}

export default CustomModal
