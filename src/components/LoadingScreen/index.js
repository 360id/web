import { LoadingCircle, MainLayout } from '../'
import Grid from '@material-ui/core/Grid'
import React from 'react'

export default React.memo(props => (
  <MainLayout {...props}>
    <Grid item>
      <LoadingCircle size={props.size || 80} />
    </Grid>
  </MainLayout>
))
