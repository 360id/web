import { OutlinedInput, InputAdornment, Button, Grid, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Search, Add } from '@material-ui/icons'
import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import { debounce } from 'lodash'

const useStyles = makeStyles(() => ({
  TableHeader: {
    marginBottom: '10px',
  },
  buttonsWrap: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  searchInputWrap: {
    padding: '8px 8px 8px 16px',
  },
  searchInput: {
    fontSize: '14px',
    padding: 0,
  },
  searchIcon: {
    paddingRight: '0px',
    color: '#c0c0c0',
  },
  title: {
    fontSize: '22px',
  },
  addButton: {
    textTransform: 'none',
    marginLeft: '16px',
    fontSize: '14px',
  },
  addIcon: {
    width: '19px',
    height: '19px',
  },
}))

const TableHeader = React.memo(({ title, onSearch, onAdd, onAddLabel }) => {
  const [searchText, $searchText] = useState('')
  const classes = useStyles()

  const debouncedOnSearch = useRef(onSearch && debounce(onSearch, 400))
  const handleSearch = e => {
    $searchText(e.target.value)
    debouncedOnSearch.current(e.target.value)
  }
  return (
    <Grid container spacing={3} className={classes.TableHeader}>
      <Grid item xs={6}>
        {title && <Typography className={classes.title}>{title}</Typography>}
      </Grid>
      <Grid item xs={6} className={classes.buttonsWrap}>
        {onSearch && (
          <OutlinedInput
            endAdornment={
              <InputAdornment className={classes.searchIcon}>
                <Search />
              </InputAdornment>
            }
            inputProps={{ className: classes.searchInput }}
            className={classes.searchInputWrap}
            placeholder='Search...'
            onChange={handleSearch}
            value={searchText}
            variant='outlined'
            color='primary'
          />
        )}
        {onAdd && (
          <Button className={classes.addButton} variant='contained' onClick={onAdd} color='primary'>
            <Add className={classes.addIcon} />
            {onAddLabel}
          </Button>
        )}
      </Grid>
    </Grid>
  )
})

TableHeader.propTypes = {
  onAddLabel: PropTypes.string,
  onSearch: PropTypes.func,
  title: PropTypes.string,
  onAdd: PropTypes.func,
}

export default TableHeader
