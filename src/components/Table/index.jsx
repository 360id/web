import {
  Checkbox,
  Table as MaUTable,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Menu,
  MenuItem,
} from '@material-ui/core'
import TablePaginationActions from './TablePaginationActions/TablePaginationActions'
import { usePagination, useRowSelect, useSortBy, useTable } from 'react-table'
import React, { useCallback, useEffect, useState } from 'react'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import { makeStyles } from '@material-ui/core/styles'
import { ConfirmDialog, LoadingCircle } from '../'
import PropTypes from 'prop-types'
import { isObject } from 'lodash'

const useActionStyles = makeStyles(theme => ({
  moreBtn: {
    cursor: 'pointer',
    position: 'relative',
    top: '2px',
  },
  moreBtnIcon: {
    fill: '#b4b4b4',
  },
}))

const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = React.useRef()
  const resolvedRef = ref || defaultRef

  React.useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate
  }, [resolvedRef, indeterminate])

  return (
    <>
      <Checkbox ref={resolvedRef} {...rest} />
    </>
  )
})

const Actions = ({ actions, row }) => {
  const onClose = useCallback(() => $isMenuOpened(null), [])
  const [isMenuOpened, $isMenuOpened] = useState(null)
  const [confirmProps, $confirmProps] = useState({})
  const classes = useActionStyles()

  const handleClick = useCallback(
    key => {
      const obj = actions[key](row)
      if (isObject(obj)) $confirmProps(obj)
      onClose()
    },
    [actions, row, onClose]
  )

  return (
    <>
      <ConfirmDialog {...confirmProps} onClose={() => $confirmProps()} />
      <span className={classes.moreBtn} onClick={e => $isMenuOpened(e.currentTarget)}>
        <MoreVertIcon className={classes.moreBtnIcon} />
      </span>
      <Menu open={Boolean(isMenuOpened)} anchorEl={isMenuOpened} onClose={onClose}>
        {Object.keys(actions).map(key => (
          <MenuItem onClick={() => handleClick(key)} key={key}>
            {key}
          </MenuItem>
        ))}
      </Menu>
    </>
  )
}

const useTableStyles = makeStyles(() => ({
  selectedRow: {
    background: '#EEF3F4',
    '& td': {
      fontWeight: 700,
      position: 'relative',
    },
  },
  selectedRowDiv: {
    background: props => props.selectedRowColor || '#3e50b4',
    position: 'absolute',
    height: '100%',
    width: '5px',
    left: 0,
    top: 0,
  },
  centerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: '10px',
    display: 'flex',
    height: '100px',
  },
}))

const Table = React.memo(
  ({
    columns,
    data,
    defaultPageCount = 0,
    defaultPageIndex = 0,
    defaultPageSize = 25,
    onPageIndexChange,
    onPageSizeChange,
    manualPagination = false,
    manualSortBy = false,
    onRowSelection,
    onSort = () => {},
    selectedRowId,
    selectedRowColor,
    actions,
    loading,
    noDataMessage,
  }) => {
    const classes = useTableStyles({ selectedRowColor })
    const checboxCol = onRowSelection
      ? [
          {
            id: 'selection',
            Header: ({ getToggleAllRowsSelectedProps }) => (
              <div>
                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              </div>
            ),
            Cell: ({ row }) => (
              <div>
                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
              </div>
            ),
          },
        ]
      : []

    const actionCol = actions
      ? [
          {
            id: 'action',
            Header: () => 'Action',
            Cell: ({ row }) => <Actions {...{ actions, row }} />,
          },
        ]
      : []

    const {
      getTableProps,
      headerGroups,
      prepareRow,
      page,
      gotoPage,
      setPageSize,
      selectedFlatRows,
      state: { sortBy, pageIndex, pageSize, selectedRowIds },
    } = useTable(
      {
        columns,
        data,
        pageCount: defaultPageCount,
        initialState: {
          pageSize: defaultPageSize,
          pageIndex: defaultPageIndex,
        },
        manualSortBy,
      },
      useSortBy,
      usePagination,
      useRowSelect,
      hooks => {
        hooks.allColumns.push(cols => [...checboxCol, ...cols, ...actionCol])
      }
    )

    useEffect(() => {
      if (onSort) onSort(sortBy)
      return
    }, [onSort, sortBy])

    useEffect(() => {
      if (onRowSelection) {
        onRowSelection(selectedRowIds, selectedFlatRows)
      }
    }, [onRowSelection, selectedFlatRows, selectedRowIds])

    const handleChangePage = (event, newPage) => {
      gotoPage(Number(newPage))
      if (manualPagination) {
        onPageIndexChange(Number(newPage))
      }
    }

    const handleChangeRowsPerPage = event => {
      setPageSize(Number(event.target.value))
      if (manualPagination) {
        onPageSizeChange(Number(event.target.value))
      }
    }

    // Render the UI for your table
    return (
      <TableContainer>
        <MaUTable {...getTableProps()}>
          <TableHead>
            {headerGroups.map(headerGroup => (
              <TableRow {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <TableCell
                    {...(column.id === 'selection'
                      ? column.getHeaderProps()
                      : column.getHeaderProps(column.getSortByToggleProps()))}>
                    {column.render('Header')}
                    {column.id !== 'selection' ? (
                      <TableSortLabel
                        active={column.isSorted}
                        // react-table has a unsorted state which is not treated here
                        direction={column.isSortedDesc ? 'desc' : 'asc'}
                      />
                    ) : null}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableHead>
          <TableBody>
            {!loading && page.length ? (
              page.map((row, i) => {
                prepareRow(row)
                const isSelected = selectedRowId && selectedRowId === row.original.id
                return (
                  <TableRow {...row.getRowProps()} className={isSelected ? classes.selectedRow : ''}>
                    {row.cells.map((cell, ind) => (
                      <TableCell {...cell.getCellProps()}>
                        {isSelected && ind === 0 && <div className={classes.selectedRowDiv} />}
                        {cell.render('Cell')}
                      </TableCell>
                    ))}
                  </TableRow>
                )
              })
            ) : (
              <TableRow>
                <TableCell colSpan={columns.length + 1}>
                  <div className={classes.centerContainer}>
                    {loading ? <LoadingCircle /> : noDataMessage || 'No data'}
                  </div>
                </TableCell>
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: data.length }]}
                colSpan={columns.length + 1}
                count={data.length}
                rowsPerPage={pageSize}
                page={pageIndex}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </MaUTable>
      </TableContainer>
    )
  }
)

Table.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  defaultPageCount: PropTypes.number,
  defaultPageIndex: PropTypes.number,
  defaultPageSize: PropTypes.number,
  onPageIndexChange: PropTypes.func,
  onPageSizeChange: PropTypes.func,
  manualPagination: PropTypes.bool,
  manualSortBy: PropTypes.bool,
  onRowSelection: PropTypes.func,
  renderActions: PropTypes.func,
  onSort: PropTypes.func,
}

Table.defaultProps = {
  defaultPageCount: 10,
  defaultPageIndex: 0,
  defaultPageSize: 10,
}

export default Table
