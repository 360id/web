import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import clname from 'classnames'
import React from 'react'

const useStyles = makeStyles(theme => ({
  statusIcon: {
    width: '14px',
    height: '14px',
    position: 'relative',
    top: '3px',
    left: '-3px',
  },
  statusIconActive: {
    fill: '#2dc756',
  },
  statusIconDraft: {
    fill: '#eab75e',
  },
  statusIconInactive: {
    fill: '#b4b4b4',
  },
}))

const Status = React.memo(({ value = 'draft' }) => {
  const classes = useStyles()
  const status = value.charAt(0).toUpperCase() + value.slice(1)
  return (
    <span>
      <FiberManualRecordIcon className={clname([classes.statusIcon], [classes[`statusIcon${status}`]])} />
      {status}
    </span>
  )
})

Status.propTypes = {
  value: PropTypes.string.isRequired,
}

export default Status
