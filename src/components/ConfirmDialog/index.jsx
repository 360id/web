import { DialogActions, DialogContent, Button, Dialog } from '@material-ui/core'
import React from 'react'

export default ({ confirm, func, onClose }) => {
  if (!confirm) return null
  return (
    <Dialog open onClose={onClose} aria-labelledby='confirm-dialog'>
      <DialogContent>{confirm}</DialogContent>
      <DialogActions>
        <Button size='small' variant='contained' onClick={onClose}>
          No
        </Button>
        <Button
          size='small'
          variant='contained'
          color='primary'
          onClick={() => {
            func && func()
            onClose()
          }}>
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  )
}
