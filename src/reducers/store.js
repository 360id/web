import { routerMiddleware } from 'react-router-redux'
import { configureStore } from '@reduxjs/toolkit'
import { createBrowserHistory } from 'history'
import reducer from './reducers'
import thunk from 'redux-thunk'

const history = createBrowserHistory()

const store = configureStore({
  middleware: [thunk, routerMiddleware(history)],
  devTools: true,
  reducer,
})

export { store, history }
