import subscriptionSlice from '../slices/subscription'
import organizationSlice from '../slices/organization'
import notificationSlice from '../slices/notification'
import { routerReducer } from 'react-router-redux'
import customerSlice from '../slices/customer'
import verifierSlice from '../slices/verifier'
import commonSlice from '../slices/common'
import authSlice from '../slices/auth'

export default {
  subscription: subscriptionSlice.reducer,
  organization: organizationSlice.reducer,
  notification: notificationSlice.reducer,
  customer: customerSlice.reducer,
  verifier: verifierSlice.reducer,
  common: commonSlice.reducer,
  auth: authSlice.reducer,
  routing: routerReducer,
}
