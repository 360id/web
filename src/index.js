import { syncHistoryWithStore } from 'react-router-redux'
import { store, history } from './reducers/store'
import * as serviceWorker from './serviceWorker'
import { Provider } from 'react-redux'
import { Router } from 'react-router'
import ReactDOM from 'react-dom'
import React from 'react'
import App from './App'

ReactDOM.render(
  <Provider store={store}>
    <Router history={syncHistoryWithStore(history, store)}>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
