import verifierService from '../services/verifierService'
import organizationSlice from './organization'
import { createSlice } from '@reduxjs/toolkit'
import { showNotification } from '../utils'

const initialState = {
  verifierMetricsList: [],
  verifierLoading: false,
}

const slice = createSlice({
  name: 'verifier',
  initialState,
  reducers: {
    handleVerifierMetricsLoading: state => {
      state.verifierMetricsLoading = !state.verifierMetricsLoading
      return state
    },
    setVerifierMetricsList: (state, { payload }) => {
      state.verifierMetricsList = payload
      return state
    },
  },
})

const thunks = {
  addUpdateVerifier: (body, organizationId) => async dispatch => {
    const res = await verifierService[body.id ? 'updateVerifier' : 'addVerifier'](body)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId))
      return res
    }
  },
  deleteVerifier: (id, organizationId) => async dispatch => {
    const res = await verifierService.deleteVerifier(id)
    if (res) {
      console.log({ res })
      showNotification({ type: 'success', message: res.message })
      dispatch(organizationSlice.thunks.getVerifiersByOrgId(organizationId))
      return res
    }
  },
  getVerifierMetrics: () => async dispatch => {
    try {
      dispatch(slice.actions.handleVerifierMetricsLoading())
      const res = await verifierService.getVerifierMetrics()
      if (res) {
        dispatch(slice.actions.setVerifierMetricsList(res.data))
        dispatch(slice.actions.handleVerifierMetricsLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleVerifierMetricsLoading())
      console.error(arror)
    }
  },
}

export default { ...slice, thunks }
