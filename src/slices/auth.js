import authService from '../services/authService'
import { createSlice } from '@reduxjs/toolkit'
import { push } from 'react-router-redux'
import { storage } from '../utils'

const initialState = {
  user: {},
  isAuth: false,
  authChecked: false,
}

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, { payload }) => {
      state.user = payload
      state.isAuth = true
      state.authChecked = true
      return state
    },
    setAuthChecked: (state, { payload }) => {
      state.authChecked = payload
      return state
    },
    logout: () => initialState,
  },
})

const thunks = {
  login: payload => async dispatch => {
    const res = await authService.login(payload)
    if (res) {
      storage.set('authToken', res.token)
      dispatch(slice.actions.login(res.user))
      dispatch(push('/'))
    }
  },
  authenticate: () => async (dispatch, getState) => {
    const { auth } = getState()
    if (auth.authChecked) return

    console.log({ authToken: storage.get('authToken') })

    if (!storage.get('authToken')) {
      dispatch(slice.actions.setAuthChecked(true))
      return
    }

    try {
      const res = await authService.me()
      if (res) dispatch(slice.actions.login(res.data))
    } catch (e) {
      console.error(e)
      dispatch(thunks.logout())
    }
  },
  logout: () => async dispatch => {
    storage.remove('authToken')
    dispatch(slice.actions.logout())
    dispatch(push('/'))
  },
}

export default { ...slice, thunks }
