import organizationService from '../services/organizationService'
import { createSlice } from '@reduxjs/toolkit'
import { showNotification } from '../utils'

const initialState = {
  singleOrganizationLoading: false,
  organizationLoading: false,
  usersMetricsLoading: false,
  verifierLoading: false,
  usersMetricsList: [],
  organizationList: [],
  verifierData: [],
  organization: {},
}

const slice = createSlice({
  name: 'organization',
  initialState,
  reducers: {
    handleSingleOrganizationLoading: state => {
      state.singleOrganizationLoading = !state.singleOrganizationLoading
      return state
    },
    handleOrganizationLoading: state => {
      state.organizationLoading = !state.organizationLoading
      return state
    },
    handleVerifierLoading: state => {
      state.verifierLoading = !state.verifierLoading
      return state
    },
    setVerifierData: (state, { payload }) => {
      state.verifierData = payload
      return state
    },
    setOrganizationList: (state, { payload }) => {
      state.organizationList = payload
      return state
    },
    setOrganization: (state, { payload }) => {
      state.organization = payload
      return state
    },
    handleUsersMetricsLoading: state => {
      state.usersMetricsLoading = !state.usersMetricsLoading
      return state
    },
    setUsersMetrics: (state, { payload }) => {
      state.usersMetricsList = payload
      return state
    },
  },
})

const thunks = {
  addUpdateOrganization: body => async dispatch => {
    const res = await organizationService[body.id ? 'updateOrganization' : 'addOrganization'](body)
    if (res) {
      dispatch(thunks.getOrganizationList())
      return res
    }
  },
  deleteOrganization: id => async dispatch => {
    const res = await organizationService.deleteOrganization(id)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getOrganizationList())
      return res
    }
  },
  getVerifiersByOrgId: id => async dispatch => {
    try {
      dispatch(slice.actions.handleVerifierLoading())
      const res = await organizationService.getOrgVerifiersById(id)
      if (res) {
        dispatch(slice.actions.setVerifierData(res.data))
        dispatch(slice.actions.handleVerifierLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleVerifierLoading())
    }
  },
  getOrganizationList: () => async dispatch => {
    try {
      dispatch(slice.actions.handleOrganizationLoading())
      const res = await organizationService.getOrganizations()
      if (res) {
        dispatch(slice.actions.setOrganizationList(res))
        dispatch(slice.actions.handleOrganizationLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleOrganizationLoading())
    }
  },
  getOrganizationById: id => async dispatch => {
    try {
      dispatch(slice.actions.handleSingleOrganizationLoading())
      const res = await organizationService.getOrganizationById(id)
      if (res) {
        dispatch(slice.actions.setOrganization(res.data))
        dispatch(slice.actions.handleSingleOrganizationLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleSingleOrganizationLoading())
    }
  },
  getUsersMetrics: () => async dispatch => {
    try {
      dispatch(slice.actions.handleUsersMetricsLoading())
      const res = await organizationService.getUsersMetrics()
      if (res) {
        dispatch(slice.actions.setUsersMetrics(res))
        dispatch(slice.actions.handleUsersMetricsLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleUsersMetricsLoading())
    }
  },
}

export default { ...slice, thunks }
