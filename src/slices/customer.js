import customerService from '../services/customerService'
import { createSlice } from '@reduxjs/toolkit'
import { showNotification } from '../utils'

const initialState = {
  customersMetricsLoading: false,
  customersMetricsList: [],
  customersLoading: false,
  customersList: [],
}

const slice = createSlice({
  name: 'customer',
  initialState,
  reducers: {
    handleCustomersLoading: state => {
      state.customersLoading = !state.customersLoading
      return state
    },
    setCustomersList: (state, { payload }) => {
      state.customersList = payload
      return state
    },
    handleCustomersMetricsLoading: state => {
      state.customersMetricsLoading = !state.customersMetricsLoading
      return state
    },
    setCustomersMetrics: (state, { payload }) => {
      state.customersMetricsList = payload
      return state
    },
  },
})

const thunks = {
  getCustomersList: () => async dispatch => {
    try {
      dispatch(slice.actions.handleCustomersLoading())
      const res = await customerService.getCustomers()
      if (res) {
        dispatch(slice.actions.setCustomersList(res))
        dispatch(slice.actions.handleCustomersLoading())
      }
    } catch (err) {
      dispatch(slice.actions.handleCustomersLoading())
    }
  },
  addUpdateCustomer: body => async dispatch => {
    const res = await customerService[body.id ? 'updateCustomer' : 'addCustomer'](body)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getCustomersList())
      return res
    }
  },
  supportCustomer: body => async dispatch => {
    const res = await customerService.supportCustomer(body)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getCustomersList())
      return res
    }
  },
  deleteCustomer: id => async dispatch => {
    const res = await customerService.removeCustomer(id)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getCustomersList())
      return res
    }
  },
  getCustomerMetrics: () => async dispatch => {
    try {
      dispatch(slice.actions.handleCustomersMetricsLoading())
      const res = await customerService.getCustomersMetrics()
      if (res) {
        dispatch(slice.actions.setCustomersMetrics(res))
        dispatch(slice.actions.handleCustomersMetricsLoading())
      }
    } catch (err) {
      dispatch(slice.actions.handleCustomersMetricsLoading())
    }
  },
}

export default { ...slice, thunks }
