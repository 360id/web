import notificationService from '../services/notificationService'
import { createSlice } from '@reduxjs/toolkit'
import { showNotification } from '../utils'

const initialState = {
  notificationsLoading: false,
  notificationsList: [],
}

const slice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    handleNotificationsListLoading: state => {
      state.notificationsLoading = !state.notificationsLoading
      return state
    },
    setNotificationsList: (state, { payload }) => {
      state.notificationsList = payload
      return state
    },
  },
})

const thunks = {
  addUpdateNotification: body => async dispatch => {
    const res = await notificationService[body.id ? 'updateNotification' : 'addNotification'](body)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getNotificationsList())
      return res
    }
  },
  deleteNotification: id => async dispatch => {
    const res = await notificationService.deleteNotification(id)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      dispatch(thunks.getNotificationsList())
      return res
    }
  },
  getNotificationsList: () => async dispatch => {
    try {
      dispatch(slice.actions.handleNotificationsListLoading())
      const res = await notificationService.getNotifications()
      if (res) {
        dispatch(slice.actions.setNotificationsList(res))
        dispatch(slice.actions.handleNotificationsListLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleNotificationsListLoading())
    }
  },
}

export default { ...slice, thunks }
