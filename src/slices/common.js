import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  notification: {
    type: 'error',
    isOpen: false,
    message: '',
  },
}

const slice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setNotification: (state, { payload }) => {
      state.notification = { ...initialState.notification, isOpen: true, ...payload }
      return state
    },
  },
})

const thunks = {
  showNotification: notification => dispatch => {
    dispatch(slice.actions.setNotification(notification))
  },
  closeNotification: (e, reason) => (dispatch, getStore) => {
    if (reason === 'clickaway') return
    dispatch(slice.actions.setNotification({ ...getStore().common.notification, isOpen: false }))
  },
}

export default { ...slice, thunks }
