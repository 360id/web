import subscriptionsService from '../services/subscriptionsService'
import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  subscriptionsListLoading: false,
  billingsListLoading: false,
  subscriptionsList: [],
  billingsList: [],
}

const slice = createSlice({
  name: 'subscription',
  initialState,
  reducers: {
    handleSubscriptionsLoading: state => {
      state.subscriptionsListLoading = !state.subscriptionsListLoading
      return state
    },
    setSubscriptionsList: (state, { payload }) => {
      state.subscriptionsList = payload
      return state
    },
    handleBillingsListLoading: state => {
      state.billingsListLoading = !state.billingsListLoading
      return state
    },
    setBillingsList: (state, { payload }) => {
      state.billingsList = payload
      return state
    },
  },
})

const thunks = {
  getSubscriptionsList: () => async dispatch => {
    try {
      dispatch(slice.actions.handleSubscriptionsLoading())
      const res = await subscriptionsService.getSubscriptions()
      if (res) {
        dispatch(slice.actions.setSubscriptionsList(res.data))
        dispatch(slice.actions.handleSubscriptionsLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleSubscriptionsLoading())
      console.error(arror)
    }
  },
  getBillingsList: () => async dispatch => {
    try {
      dispatch(slice.actions.handleBillingsListLoading())
      const res = await subscriptionsService.getBillingMethods()
      if (res) {
        dispatch(slice.actions.setBillingsList(res.data))
        dispatch(slice.actions.handleBillingsListLoading())
      }
    } catch (arror) {
      dispatch(slice.actions.handleBillingsListLoading())
      console.error(arror)
    }
  },
}

export default { ...slice, thunks }
