import { get } from 'lodash'

export default err => {
  if (err.response) {
    const respError = get(err, 'response.data.message')
    return `Error: ${respError || `${err.response.status} ${err.response.statusText}`}`
  }

  return `Error: ${err.message}`
}
