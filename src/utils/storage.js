export default {
  set: (key, data) => localStorage.setItem(key, data),
  remove: key => localStorage.removeItem(key),
  get: key => localStorage.getItem(key),
}
