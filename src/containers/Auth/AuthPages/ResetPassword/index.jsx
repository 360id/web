import authService from '../../../../services/authService'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Container } from '@material-ui/core'
import { useHistory, useParams } from 'react-router'
import { showNotification } from '../../../../utils'
import { TextField } from 'formik-material-ui'
import { Formik, Field, Form } from 'formik'
import * as Yup from 'yup'
import React from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    width: '350px',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    textTransform: 'none',
  },
  textLabel: {
    color: '#3894F0',
  },
  textInput: {
    backgroundColor: 'transparent',
    color: '#fff',
    '&:before': {
      borderBottomColor: '#3894F0',
    },
    '&:hover:not(.Mui-disabled):before': {
      borderBottomColor: '#3894F0',
    },
  },
}))

const validation = Yup.object().shape({
  password: Yup.string()
    .required('Required!')
    .min(8, 'Password is too short - should be 8 chars minimum!')
    .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
  confirmPassword: Yup.string()
    .required('Required!')
    .oneOf([Yup.ref('password'), null], 'Passwords must match!'),
})

export default React.memo(() => {
  const { resetToken } = useParams()
  const history = useHistory()
  const classes = useStyles()

  const onSubmit = async values => {
    const res = await authService.reset(resetToken, values.password)
    if (res) {
      showNotification({ type: 'success', message: 'Password set successfully!' })
      history.push('/auth/sign-in')
    }
  }

  const defaultProps = {
    InputLabelProps: { classes: { root: classes.textLabel } },
    InputProps: { classes: { root: classes.textInput } },
    className: 'fieldWrap',
    component: TextField,
    margin: 'normal',
    fullWidth: true,
    required: true,
  }

  return (
    <Container component='main' className={classes.root}>
      <Formik initialValues={{ password: '', confirmPassword: '' }} validationSchema={validation} onSubmit={onSubmit}>
        {() => (
          <Form className={classes.form} noValidate>
            <Field {...defaultProps} autoComplete='current-password' label='Password' name='password' type='password' />
            <Field
              {...defaultProps}
              autoComplete='current-password'
              label='Confirm password'
              name='confirmPassword'
              type='password'
            />
            <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
              Set Password
            </Button>
          </Form>
        )}
      </Formik>
    </Container>
  )
})
