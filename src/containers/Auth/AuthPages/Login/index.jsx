import { Button, Container } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { TextField } from 'formik-material-ui'
import userSlice from '../../../../slices/auth'
import { Formik, Field, Form } from 'formik'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import * as Yup from 'yup'
import React from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    width: '350px',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    textTransform: 'none',
  },
  textLabel: {
    color: '#3894F0',
  },
  textInput: {
    backgroundColor: 'transparent',
    color: '#fff',
    '&:before': {
      borderBottomColor: '#3894F0',
    },
    '&:hover:not(.Mui-disabled):before': {
      borderBottomColor: '#3894F0',
    },
  },
  orLabel: {
    textAlign: 'center',
    marginBottom: '5px',
    color: '#fff',
  },
  linkWrap: {
    textAlign: 'center',
  },
  link: {
    color: '#368EE6',
  },
}))

const LoginValidations = Yup.object().shape({
  email: Yup.string().email('Invalid Email!').required('Required!'),
  password: Yup.string()
    .required('Required!')
    .min(8, 'Password is too short - should be 8 chars minimum!')
    .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
})

export default React.memo(() => {
  const dispatch = useDispatch()
  const classes = useStyles()

  const onSubmit = values => dispatch(userSlice.thunks.login(values))

  const defaultProps = {
    InputLabelProps: { classes: { root: classes.textLabel } },
    InputProps: { classes: { root: classes.textInput } },
    className: 'fieldWrap',
    component: TextField,
    margin: 'normal',
    fullWidth: true,
    required: true,
  }

  return (
    <Container component='main' className={classes.root}>
      <Formik initialValues={{ email: '', password: '' }} validationSchema={LoginValidations} onSubmit={onSubmit}>
        {() => (
          <Form className={classes.form} noValidate>
            <Field {...defaultProps} label='Email' name='email' autoComplete='email' type='email' autoFocus />
            <Field {...defaultProps} autoComplete='current-password' label='Password' name='password' type='password' />
            <Link to='/auth/forgot-password' className={classes.link}>
              Forgot Password?
            </Link>
            <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
              Login
            </Button>
            <div className={classes.orLabel}>or</div>
            <div className={classes.linkWrap}>
              <Link to='/auth/sign-up' className={classes.link}>
                Create an Account
              </Link>
            </div>
          </Form>
        )}
      </Formik>
    </Container>
  )
})
