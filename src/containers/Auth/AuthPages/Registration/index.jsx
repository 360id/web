import { Grid, Button, Container, InputLabel, Select, MenuItem, FormControl } from '@material-ui/core'
import { Link, useHistory, useLocation } from 'react-router-dom'
import authService from '../../../../services/authService'
import { makeStyles } from '@material-ui/core/styles'
import { showNotification } from '../../../../utils'
import { flattenDeep, pick, omit } from 'lodash'
import { TextField } from 'formik-material-ui'
import { USER } from '../../../../constants'
import { Formik, Field, Form } from 'formik'
import React, { memo, useMemo } from 'react'
import * as Yup from 'yup'

const MAIN_BODY_KEYS = ['email', 'password', 'confirmPassword']

const FIELD_POS_SCHEME = {
  organization: {
    left: ['firstName', 'organization', 'password', 'masterKey'],
    right: ['lastName', 'email', 'confirmPassword', 'phoneNumber'],
  },
  individual: {
    left: ['firstName', 'masterKey', 'password', 'phoneNumber'],
    right: ['lastName', 'email', 'confirmPassword'],
  },
  default: {
    left: ['firstName', 'email', 'password'],
    right: ['lastName', 'phoneNumber', 'confirmPassword'],
  },
}

const useStyles = makeStyles(theme => ({
  root: {
    width: '650px',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  textLabel: {
    color: '#3894F0',
  },
  textInput: {
    backgroundColor: 'transparent',
    color: '#fff',
    '&:before': {
      borderBottomColor: '#3894F0',
    },
    '&:hover:not(.Mui-disabled):before': {
      borderBottomColor: '#3894F0',
    },
    '& svg': {
      fill: '#3894F0',
    },
  },
  buttonWrap: {
    display: 'flex',
    justifyContent: 'center',
  },
  submit: {
    margin: '24px auto 16px',
    textTransform: 'none',
    width: '280px',
  },
  linkWrap: {
    textAlign: 'center',
  },
  link: {
    color: '#368EE6',
  },
}))

const validationSchema = {
  firstName: Yup.string().required('Required!'),
  lastName: Yup.string().required('Required!'),
  email: Yup.string().email('Invalid Email').required('Required!'),
  organization: Yup.string().required('Required!'),
  masterKey: Yup.string().required('Required!'),
  phoneNumber: Yup.string().required('Required!'),
  mobileProvider: Yup.string().required('Required!'),
  password: Yup.string()
    .required('Required!')
    .min(8, 'Password is too short - should be 8 chars minimum!')
    .matches(/^[A-Za-z0-9_]+$/, 'Password can only contain Latin letters and numbers!'),
  confirmPassword: Yup.string()
    .required('Required!')
    .oneOf([Yup.ref('password'), null], 'Passwords must match!'),
}

export default memo(() => {
  const location = useLocation()
  const history = useHistory()
  const classes = useStyles()

  const userRole = location.pathname.split('/').pop()
  const fieldKeys = FIELD_POS_SCHEME[userRole] || FIELD_POS_SCHEME.default
  const validationKeys = flattenDeep(Object.keys(fieldKeys).map(key => fieldKeys[key]))

  const fieldList = useMemo(() => {
    const defaultProps = {
      InputLabelProps: { classes: { root: classes.textLabel } },
      InputProps: { classes: { root: classes.textInput } },
      className: 'fieldWrap',
      component: TextField,
      margin: 'normal',
      fullWidth: true,
      required: true,
    }
    return {
      email: () => fieldList.default({ label: 'Email', name: 'email', autoComplete: 'email', type: 'email' }),
      organization: () => fieldList.default({ label: 'Organization', name: 'organization' }),
      firstName: () => fieldList.default({ label: 'First name', name: 'firstName' }),
      masterKey: () => fieldList.default({ label: 'Master Key', name: 'masterKey' }),
      lastName: () => fieldList.default({ label: 'Last name', name: 'lastName' }),
      confirmPassword: () =>
        fieldList.default({
          autoComplete: 'current-password',
          label: 'Confirm password',
          name: 'confirmPassword',
          type: 'password',
        }),
      password: () =>
        fieldList.default({
          autoComplete: 'current-password',
          label: 'Password',
          name: 'password',
          type: 'password',
        }),
      phoneNumber: ({ setFieldValue, values }) => (
        <Grid container spacing={3} key='phoneNumber'>
          <Grid item xs={8}>
            <Field {...defaultProps} label='Mobile Number' name='phoneNumber' type='number' />
          </Grid>
          <Grid item xs={4}>
            <FormControl fullWidth margin='normal'>
              <InputLabel className={classes.textLabel}>Provider</InputLabel>
              <Select
                onChange={e => setFieldValue('mobileProvider', e.target.value)}
                className={classes.textInput}
                value={values.mobileProvider || ''}
                name='mobileProvider'>
                {USER.MOBILE_PROVIDERS.map(({ label, value }) => (
                  <MenuItem key={value} value={value}>
                    {label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      ),
      default: props => <Field {...defaultProps} {...props} key={props.name} />,
    }
  }, [classes])

  const getInitialValues = () => {
    const values = {}
    validationKeys.forEach(key => {
      values[key] = ''
    })
    return values
  }

  const onSubmit = async values => {
    const body = {
      ...pick(values, MAIN_BODY_KEYS),
      data: omit(values, MAIN_BODY_KEYS),
    }
    const res = await authService.register(body)
    if (res) {
      showNotification({ type: 'success', message: res.message })
      history.push('/auth/sign-up')
    }
  }

  return (
    <Container component='main' className={classes.root}>
      <Formik
        validationSchema={Yup.object().shape(pick(validationSchema, validationKeys))}
        initialValues={getInitialValues()}
        onSubmit={onSubmit}>
        {props => (
          <Form className={classes.form} noValidate>
            <Grid container spacing={3}>
              {Object.keys(fieldKeys).map(topKey => (
                <Grid item xs={6} key={topKey}>
                  {fieldKeys[topKey].map(key => fieldList[key] && fieldList[key](props))}
                </Grid>
              ))}
            </Grid>
            <div className={classes.buttonWrap}>
              <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
                Register
              </Button>
            </div>
            <div className={classes.linkWrap}>
              <Link to='/auth/sign-in' className={classes.link}>
                Already registered? Login
              </Link>
            </div>
          </Form>
        )}
      </Formik>
    </Container>
  )
})
