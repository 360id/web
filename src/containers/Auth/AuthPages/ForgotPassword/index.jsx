import authService from '../../../../services/authService'
import { Button, Container } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { showNotification } from '../../../../utils'
import { TextField } from 'formik-material-ui'
import { Formik, Field, Form } from 'formik'
import { useHistory } from 'react-router'
import * as Yup from 'yup'
import React from 'react'

const useStyles = makeStyles(theme => ({
  root: {
    width: '350px',
    height: '100vh',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    textTransform: 'none',
  },
  textLabel: {
    color: '#3894F0',
  },
  textInput: {
    backgroundColor: 'transparent',
    color: '#fff',
    '&:before': {
      borderBottomColor: '#3894F0',
    },
    '&:hover:not(.Mui-disabled):before': {
      borderBottomColor: '#3894F0',
    },
  },
}))

const EmailValidations = Yup.object().shape({
  email: Yup.string().email('Invalid Email!').required('Required!'),
})

export default React.memo(() => {
  const history = useHistory()
  const classes = useStyles()

  const onSubmit = async values => {
    const res = await authService.forgot(values)
    if (res) {
      showNotification({ type: 'success', message: res.data.message })
      history.push('/') // todo upd url
    }
  }

  return (
    <Container component='main' className={classes.root}>
      <Formik initialValues={{ email: '' }} validationSchema={EmailValidations} onSubmit={onSubmit}>
        {() => (
          <Form className={classes.form} noValidate>
            <Field
              InputLabelProps={{ classes: { root: classes.textLabel } }}
              InputProps={{ classes: { root: classes.textInput } }}
              className='fieldWrap'
              component={TextField}
              autoComplete='email'
              margin='normal'
              label='Email'
              name='email'
              fullWidth
              autoFocus
              required
            />
            <Button className={classes.submit} type='submit' fullWidth variant='contained' color='primary'>
              Reset Password
            </Button>
          </Form>
        )}
      </Formik>
    </Container>
  )
})
