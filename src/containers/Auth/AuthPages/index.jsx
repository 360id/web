import { makeStyles } from '@material-ui/core/styles'
import { AnimatedSwitch } from '../../../components'
import { Container, Grid } from '@material-ui/core'
import ForgotPassword from './ForgotPassword'
import ResetPassword from './ResetPassword'
import Registration from './Registration'
import Login from './Login'
import React from 'react'

const useStyles = makeStyles(() => ({
  root: {
    padding: 0,
    backgroundImage: 'url("/authBackground.png")',
    backgroundSize: 'cover',
    maxWidth: '100%',
  },
  leftBar: {
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'rgba(255, 255, 255, 0.1)',
  },
  bigLogo: {
    width: '400px',
  },
  rightBar: {
    height: '100vh',
  },
}))

const routes = [
  { exact: true, path: '/auth/sign-in', component: Login },
  { exact: true, path: '/auth/sign-up', component: Registration },
  { exact: true, path: '/auth/sign-up/organization', component: Registration },
  { exact: true, path: '/auth/sign-up/individual', component: Registration },
  { exact: true, path: '/auth/sign-up/customer', component: Registration },
  { exact: true, path: '/auth/forgot-password', component: ForgotPassword },
  { exact: true, path: '/auth/reset-password/:resetToken', component: ResetPassword },
]

export default React.memo(() => {
  const classes = useStyles()
  return (
    <Container className={classes.root}>
      <Grid container>
        <Grid item xs={6} className={classes.leftBar}>
          <img src='/bigLogo.png' alt='bigLogo' className={classes.bigLogo} />
        </Grid>
        <Grid item xs={6} className={classes.rightBar}>
          <AnimatedSwitch routes={routes} isNotFoundRoute />
        </Grid>
      </Grid>
    </Container>
  )
})
