import { AnimatedSwitch, ErrorBoundary } from '../../components'
import AuthPages from './AuthPages'
import MainPage from './MainPage'
import React from 'react'

const routes = [
  { exact: true, path: '/', component: MainPage },
  { exact: false, path: '/auth', component: AuthPages },
]

export default React.memo(() => (
  <ErrorBoundary>
    <AnimatedSwitch routes={routes} isNotFoundRoute />
  </ErrorBoundary>
))
