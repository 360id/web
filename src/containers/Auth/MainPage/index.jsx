import { AppBar, Box, Card, CardContent, Button } from '@material-ui/core'
import { MainLayout, MainTopBar } from '../../../components'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import React from 'react'

const useStyles = makeStyles(() => ({
  mainLayout: {
    backgroundImage: 'url("/mainBackground.png")',
    height: 'calc(100vh - 64px)',
    backgroundSize: 'cover',
    paddingBottom: '100px',
    maxWidth: '100%',
  },
  cardRoot: {
    margin: '0px 20px',
    height: '280px',
    width: '340px',
    background: 'rgba(255,255,255,.7)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: '110px',
    height: '120px',
  },
  link: {
    textDecoration: 'none',
  },
  btn: {
    textTransform: 'none',
    marginTop: '25px',
    width: '210px',
  },
}))

const MENU_ITEMS = [
  {
    title: 'Register your Organization',
    path: '/auth/sign-up/organization',
    icon: '/organization.svg',
  },
  {
    path: '/auth/sign-up/individual',
    title: 'Register as Individual',
    icon: '/individual.svg',
  },
]

export default React.memo(() => {
  const classes = useStyles()
  return (
    <Box>
      <AppBar position='static'>
        <MainTopBar isNoUser />
      </AppBar>
      <MainLayout isDefaultPadding noSpacing className={classes.mainLayout}>
        {MENU_ITEMS.map(({ title, icon, path }) => (
          <Card className={classes.cardRoot} key={title}>
            <CardContent className={classes.cardContent}>
              <img src={icon} alt={icon} className={classes.img} />
              <Link to={path} className={classes.link}>
                {' '}
                <Button variant='contained' color='primary' className={classes.btn}>
                  {title}
                </Button>
              </Link>
            </CardContent>
          </Card>
        ))}
      </MainLayout>
    </Box>
  )
})
