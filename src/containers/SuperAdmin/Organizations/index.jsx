import { Table, TableHeader, MainLayout, Status } from '../../../components'
import AddEditOrganizationModal from './AddEditOrganizationModal'
import organizationSlice from '../../../slices/organization'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import React, { useEffect, useState } from 'react'
import { Paper, Grid } from '@material-ui/core'
import { Link } from 'react-router-dom'

const useStyles = makeStyles(() => ({
  statusIcon: {
    width: '14px',
    height: '14px',
    position: 'relative',
    top: '3px',
    left: '-3px',
  },
  statusIconActive: {
    fill: '#2dc756',
  },
  statusIconDraft: {
    fill: '#eab75e',
  },
  statusIconInactive: {
    fill: '#b4b4b4',
  },
  singleOrgLink: {
    color: '#2177c1',
    textDecoration: 'none',
    fontWeight: 700,
  },
}))

export default React.memo(() => {
  const { organizationLoading, organizationList } = useSelector(state => state.organization)
  const [isAddEditModal, $isAddEditModal] = useState(false)
  const [rowToEdit, $rowToEdit] = useState(null)
  const dispatch = useDispatch()
  const classes = useStyles()

  useEffect(() => {
    dispatch(organizationSlice.thunks.getOrganizationList())
  }, [dispatch])

  const columns = React.useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'businessName',
        Cell: ({ value, row }) => (
          <Link className={classes.singleOrgLink} to={`/organizations/${row.original.id}`}>
            {value}
          </Link>
        ),
      },
      {
        Header: 'Subscription',
        accessor: 'subscription',
      },
      {
        Header: 'Members',
        accessor: 'users',
        Cell: ({ value = [] }) => value.length,
      },
      {
        Header: 'Administrator',
        accessor: 'administrator',
      },
      {
        Header: 'Email',
        accessor: 'data.email',
      },
      {
        Header: 'Phone',
        accessor: 'mobile',
      },
      {
        Header: 'Master Key',
        accessor: 'masterKey',
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: ({ value }) => <Status value={value} />,
      },
    ],
    [classes.singleOrgLink]
  )

  const onSearch = searchText => {
    dispatch(organizationSlice.thunks.getOrganizationList({ searchText }))
  }

  const onAddEdit = row => {
    $rowToEdit(row)
    $isAddEditModal(!isAddEditModal)
  }

  const actions = {
    Edit: row => onAddEdit(row.original),
    Delete: row => ({
      confirm: `Are you sure you want to delete ${row.original.businessName}`,
      func: () => dispatch(organizationSlice.thunks.deleteOrganization(row.original.id)),
    }),
  }

  return (
    <MainLayout isDefaultPadding>
      {isAddEditModal && <AddEditOrganizationModal onCancel={() => onAddEdit()} rowToEdit={rowToEdit} />}
      <Grid item xs={12}>
        <TableHeader
          title='Organizations'
          onSearch={onSearch}
          onAdd={() => onAddEdit()}
          onAddLabel='Add Organization'
        />
        <Paper>
          <Table loading={organizationLoading} data={organizationList} actions={actions} columns={columns} />
        </Paper>
      </Grid>
    </MainLayout>
  )
})
