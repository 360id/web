import { Typography, Container, Grid, Button } from '@material-ui/core'
import organizationSlice from '../../../../slices/organization'
import { makeStyles } from '@material-ui/core/styles'
import { showNotification } from '../../../../utils'
import React, { useRef, useState } from 'react'
import { Modal } from '../../../../components'
import { TextField } from 'formik-material-ui'
import { Formik, Field, Form } from 'formik'
import { useDispatch } from 'react-redux'
import { Add } from '@material-ui/icons'
import { pick, omit } from 'lodash'
import clname from 'classnames'
import * as Yup from 'yup'

const useStyles = makeStyles(() => ({
  container: {
    width: '630px',
  },
  flexBottom: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  masterKey: {
    background: '#ebebeb',
    borderRadius: '15px',
    padding: '0px 20px 20px 20px',
  },
  masterKeyBtn: {
    background: '#3fe5ef',
    textTransform: 'none',
    fontSize: '14px',
    width: '100%',
    marginTop: '10px',
  },
  typography: {
    marginTop: '10px',
    color: '#b4b4b4',
    fontSize: '14px',
    fontWeight: 600,
  },
  typographyKey: {
    marginBottom: '20px',
  },
  InputProps: {
    fontSize: '14px',
  },
  addButtonsWrap: {
    marginLeft: '16px',
  },
  addButton: {
    textTransform: 'none',
    marginLeft: '16px',
    fontSize: '14px',
  },
}))

const initialValues = {
  businessName: '',
  addressLine1: '',
  firstName: '',
  masterKey: '',
  lastName: '',
  state: '',
  email: '',
  city: '',
  zip: '',
}

const validation = Yup.object().shape({
  email: Yup.string().email('Invalid Email').required('Required'),
  businessName: Yup.string().required('Required'),
  addressLine1: Yup.string().required('Required'),
  firstName: Yup.string().required('Required'),
  masterKey: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  state: Yup.string().required('Required'),
  city: Yup.string().required('Required'),
  zip: Yup.string().required('Required'),
})

export default React.memo(({ onCancel, rowToEdit }) => {
  const [isSendEmail, $isSendEmail] = useState(false)
  const dispatch = useDispatch()
  const classes = useStyles()
  const myFormRef = useRef()
  const keyRef = useRef()

  const generateKey = setFieldValue => {
    setFieldValue('masterKey', Math.random().toString(36).substr(2, 9))
    keyRef.current.focus()
  }

  const handleSave = isSnd => {
    $isSendEmail(isSnd)
    myFormRef.current.handleSubmit()
  }

  const renderButtons = () =>
    rowToEdit ? (
      <div className={classes.addButtonsWrap}>
        <Button className={classes.addButton} variant='contained' color='primary' onClick={handleSave}>
          Save
        </Button>
      </div>
    ) : (
      <div className={classes.addButtonsWrap}>
        <Button className={classes.addButton} variant='contained' onClick={() => handleSave(true)} color='primary'>
          <Add className={classes.addIcon} />
          Add organization and send key by email
        </Button>
        <Button className={classes.addButton} variant='outlined' onClick={handleSave}>
          Save as Draft
        </Button>
      </div>
    )

  const onSubmit = async values => {
    let body = {
      ...pick(values, ['businessName', 'masterKey']),
      status: isSendEmail ? 'inactive' : 'draft',
      data: {
        ...pick(values, ['firstName', 'lastName', 'email', 'city', 'state', 'zip']),
        businessAddress: pick(values, ['addressLine1', 'addressLine2']),
      },
      ...(rowToEdit ? { id: rowToEdit.id } : {}),
    }
    if (rowToEdit) {
      body = {
        ...(rowToEdit ? { id: rowToEdit.id } : {}),
        ...omit(body, 'masterKey'),
      }
    }
    const res = await dispatch(organizationSlice.thunks.addUpdateOrganization(body))
    if (res) {
      showNotification({ type: 'success', message: res.message })
      onCancel()
    }
  }

  const getInitialValues = () => {
    if (!rowToEdit) return initialValues
    return {
      ...initialValues,
      ...pick(rowToEdit, ['businessName', 'masterKey']),
      ...pick(rowToEdit.data, ['firstName', 'lastName', 'email', 'city', 'state', 'zip']),
      ...pick(rowToEdit.data.businessAddress, ['addressLine1', 'addressLine2']),
    }
  }

  const defaultProps = {
    InputProps: { classes: { input: classes.InputProps } },
    component: TextField,
    margin: 'normal',
    fullWidth: true,
    required: true,
    type: 'text',
  }
  return (
    <Modal
      title={`${rowToEdit ? 'Edit' : 'Add'} organization`}
      customButtons={renderButtons()}
      onCancel={onCancel}
      buttonsPos='start'
      dividers={false}
      isOpen>
      <Container className={classes.container}>
        <div>
          <Formik
            initialValues={getInitialValues()}
            validationSchema={validation}
            innerRef={myFormRef}
            onSubmit={onSubmit}>
            {({ setFieldValue }) => (
              <Form noValidate>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Typography className={classes.typography}>Organization Information</Typography>
                    <Field {...defaultProps} label='Business name' name='businessName' />
                    <Field {...defaultProps} label='Address Line 1' name='addressLine1' />
                    <Field {...defaultProps} label='City' name='city' />
                  </Grid>
                  <Grid className={classes.flexBottom} item xs={6}>
                    <Field {...defaultProps} label='Address Line 2' name='addressLine2' required={false} />
                    <Grid container spacing={3}>
                      <Grid item xs={6}>
                        <Field {...defaultProps} label='State' name='state' />
                      </Grid>
                      <Grid item xs={6}>
                        <Field {...defaultProps} label='Zip Code' name='zip' />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Typography className={classes.typography}>Administrator Information</Typography>
                    <Field {...defaultProps} label='First Name' name='firstName' />
                    <Field {...defaultProps} label='Last Name' name='lastName' />
                    <Field {...defaultProps} label='Email Address' name='email' />
                  </Grid>
                  <Grid item xs={6}>
                    <Typography className={clname([classes.typography], [classes.typographyKey])}>
                      Master Key Information
                    </Typography>
                    <div className={classes.masterKey}>
                      <Field {...defaultProps} label='Master Key' name='masterKey' inputRef={keyRef} />
                      <Button
                        onClick={() => generateKey(setFieldValue)}
                        className={classes.masterKeyBtn}
                        variant='contained'
                        color='primary'>
                        Generate Master Key
                      </Button>
                    </div>
                  </Grid>
                </Grid>
              </Form>
            )}
          </Formik>
        </div>
      </Container>
    </Modal>
  )
})
