import { Container, Button, Grid, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core'
import customerSlice from '../../../../slices/customer'
import { makeStyles } from '@material-ui/core/styles'
import { Modal } from '../../../../components'
import { TextField } from 'formik-material-ui'
import { Formik, Field, Form } from 'formik'
import { USER } from '../../../../constants'
import { useDispatch } from 'react-redux'
import React, { useRef } from 'react'
import { pick } from 'lodash'
import * as Yup from 'yup'

const useStyles = makeStyles(theme => ({
  container: {
    width: '630px',
  },
  addButtonsWrap: {
    marginLeft: '16px',
  },
  addButton: {
    textTransform: 'none',
    marginLeft: '16px',
    fontSize: '14px',
  },
}))

const validationSchema = {
  email: Yup.string().email('Invalid Email').required('Required!'),
  phoneNumber: Yup.string().required('Required!'),
  mobileProvider: Yup.string().required('Required!'),
  firstName: Yup.string().required('Required!'),
  lastName: Yup.string().required('Required!'),
}

export default React.memo(({ onCancel, rowToEdit }) => {
  const dispatch = useDispatch()
  const classes = useStyles()
  const myFormRef = useRef()

  const renderButtons = () => (
    <div className={classes.addButtonsWrap}>
      <Button
        onClick={() => myFormRef.current.handleSubmit()}
        className={classes.addButton}
        variant='contained'
        color='primary'>
        Save
      </Button>
      <Button className={classes.addButton} variant='outlined' onClick={onCancel}>
        Cancel
      </Button>
    </div>
  )

  const onSubmit = async values => {
    const body = {
      ...pick(values, ['email']),
      data: pick(values, ['firstName', 'lastName', 'phoneNumber', 'mobileProvider']),
      ...(rowToEdit ? { id: rowToEdit.id } : {}),
    }
    const res = await dispatch(customerSlice.thunks.addUpdateCustomer(body))
    if (res) onCancel()
  }

  const getInitialValues = () => {
    if (rowToEdit) {
      return {
        ...pick(rowToEdit, ['email']),
        ...pick(rowToEdit.data, ['firstName', 'lastName', 'phoneNumber', 'mobileProvider']),
      }
    }
    const values = {}
    Object.keys(validationSchema).forEach(key => {
      values[key] = ''
    })
    return values
  }

  const defaultProps = {
    component: TextField,
    margin: 'normal',
    fullWidth: true,
    required: true,
  }

  return (
    <Modal
      title={`${rowToEdit ? 'Edit' : 'Add'} Customer`}
      customButtons={renderButtons()}
      onCancel={onCancel}
      buttonsPos='start'
      dividers={false}
      isOpen>
      <Container className={classes.container}>
        <Formik
          validationSchema={Yup.object().shape(validationSchema)}
          initialValues={getInitialValues()}
          innerRef={myFormRef}
          onSubmit={onSubmit}>
          {({ values, setFieldValue, errors, touched }) => (
            <Form noValidate>
              <Grid container spacing={3}>
                <Grid item xs={6}>
                  <Field {...defaultProps} label='Email' name='email' autoComplete='email' type='email' />
                  <Field {...defaultProps} label='First name' name='firstName' />
                </Grid>
                <Grid item xs={6}>
                  <Grid container spacing={3}>
                    <Grid item xs={8}>
                      <Field {...defaultProps} label='Mobile Number' name='phoneNumber' type='number' />
                    </Grid>
                    <Grid item xs={4}>
                      <FormControl fullWidth margin='normal'>
                        <InputLabel className={classes.textLabel}>Provider</InputLabel>
                        <Select
                          onChange={e => setFieldValue('mobileProvider', e.target.value)}
                          value={values.mobileProvider || ''}
                          name='mobileProvider'>
                          {USER.MOBILE_PROVIDERS.map(({ label, value }) => (
                            <MenuItem key={value} value={value}>
                              {label}
                            </MenuItem>
                          ))}
                        </Select>
                      </FormControl>
                    </Grid>
                  </Grid>
                  <Field {...defaultProps} label='Last name' name='lastName' />
                </Grid>
              </Grid>
            </Form>
          )}
        </Formik>
      </Container>
    </Modal>
  )
})
