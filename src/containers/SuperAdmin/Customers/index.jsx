import { Table, TableHeader, MainLayout } from '../../../components'
import RecentActorsIcon from '@material-ui/icons/RecentActors'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import AddEditCustomerModal from './AddEditCustomerModal'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import customerSlice from '../../../slices/customer'
import { Paper, Grid } from '@material-ui/core'
import React, { useEffect, useState } from 'react'

const useStyles = makeStyles(() => ({
  iconWrap: {
    display: 'flex',
    justifyContent: 'center',
  },
  iconDriver: {
    fontSize: '28px',
    color: '#3895f1',
  },
  iconFacial: {
    fontSize: '32px',
    color: '#3895f1',
  },
}))

export default React.memo(() => {
  const { customersLoading, customersList } = useSelector(state => state.customer)
  const [isAddEditModal, setIsAddEditModal] = useState(false)
  const [rowToEdit, setRowToEdit] = useState(null)
  const dispatch = useDispatch()
  const classes = useStyles()

  useEffect(() => {
    dispatch(customerSlice.thunks.getCustomersList())
  }, [dispatch])

  const columns = React.useMemo(
    () => [
      {
        Header: 'First Name',
        accessor: 'data.firstName',
      },
      {
        Header: 'Last Name',
        accessor: 'data.lastName',
      },
      {
        Header: `Driver's Lisence`,
        accessor: 'driverLicence',
        Cell: () => (
          <div className={classes.iconWrap}>
            <AccountBoxIcon className={classes.iconDriver} />
          </div>
        ),
      },
      {
        Header: 'Facial Record',
        accessor: 'facialRecords',
        Cell: () => (
          <div className={classes.iconWrap}>
            <RecentActorsIcon className={classes.iconFacial} />
          </div>
        ),
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'Phone',
        accessor: 'data.phoneNumber',
        Cell: ({ value }) => value || '-',
      },
    ],
    [classes.iconWrap, classes.iconDriver, classes.iconFacial]
  )

  const onSearch = searchText => {
    dispatch(customerSlice.thunks.getCustomersList({ searchText }))
  }

  const onAddEdit = row => {
    setRowToEdit(row)
    setIsAddEditModal(!isAddEditModal)
  }

  const actions = {
    Edit: row => onAddEdit(row.original),
    Delete: row => ({
      confirm: `Are you sure you want to delete ${row.original.data.firstName} ${row.original.data.lastName}`,
      func: () => dispatch(customerSlice.thunks.deleteCustomer(row.original.id)),
    }),
  }

  return (
    <MainLayout isDefaultPadding>
      {isAddEditModal && <AddEditCustomerModal onCancel={() => onAddEdit()} rowToEdit={rowToEdit} />}
      <Grid item xs={12}>
        <TableHeader title='Customers' onSearch={onSearch} onAdd={() => onAddEdit()} onAddLabel='Add Customer' />
        <Paper>
          <Table loading={customersLoading} data={customersList} columns={columns} actions={actions} />
        </Paper>
      </Grid>
    </MainLayout>
  )
})
