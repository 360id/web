const ROLES = {
  INDIVIDUAL_VERIFIER: 'individual_verifier',
  ADMIN_VERIFIER: 'verifier_admin',
  SUPER_ADMIN: 'super_admin',
  USER: 'user',
}

const MOBILE_PROVIDERS = [
  { label: 'USA', value: 'usa' },
  { label: 'Verizon', value: 'verizon' },
]

export default { ROLES, MOBILE_PROVIDERS }
